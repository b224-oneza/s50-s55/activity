import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Register() {

	const { user } = useContext(UserContext);
	const navigate = useNavigate(UserContext);

	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [email, setEmail] = useState('');
	const [ mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether the submit button is enabled or not.
	const [isActive, setIsActive] = useState(false);

	console.log(email)
	console.log(password1)
	console.log(password2)


	// Function to simulate user registration
	function registerUser(e) {
		
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			if(data !== true ) {
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						isAdmin: false,
						mobileNo: mobileNo
					})
				})
				.then(res => res.json())
				.then(data => {

					console.log(data)

				})

				Swal.fire({
					title: "Registration successfull",
					icon: "success",
					text: "Welcome to Zuitt!"
				})
				navigate("/login")

			} else {

				Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Please provide a different email.",
				});
			}

		})

		// Clear the input fields and states
		setFirstName("");
		setLastName("");
		setEmail("");
		setMobileNo("");
		setPassword1("");
		setPassword2(""); 

		// alert("Thank you for registering!")
	}


	useEffect(() => {
		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo.length == 11 && password1 !== "" && password2 !== "") && (password1 === password2)) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName , email, mobileNo, password1, password2]);


	return user.id !== null ? (
    <Navigate to="/courses" />
  ) : (
    <Form onSubmit={(e) => registerUser(e)}>
      <Form.Group className="mb-3">
        <Form.Label>First Name</Form.Label>
        <Form.Control
          type="firstName"
          placeholder="Enter first name"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3">
        <Form.Label>Last Name</Form.Label>
        <Form.Control
          type="lastName"
          placeholder="Enter last name"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control
          type="mobileNo"
          placeholder="Enter mobile number"
          value={mobileNo}
          onChange={(e) => setMobileNo(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password1}
          onChange={(e) => setPassword1(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password2">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Verify Password"
          value={password2}
          onChange={(e) => setPassword2(e.target.value)}
          required
        />
      </Form.Group>

      {isActive ? (
        <Button variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
      ) : (
        <Button variant="primary" type="submit" id="submitBtn" disabled>
          Submit
        </Button>
      )}
    </Form>
  );

}